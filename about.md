---
layout: post
title: about
permalink: /about/
---

# About

**NOTICE:** This site has been re-done at http://furrz.github.io. Check that out
instead. Thanks!

## Who Am I?

I am a 14-year-old furry from Toronto, Canada! I’m male and gay! I’m available right now!

## Where to Find Me

**Main Blog:** [@furrz](http://furrz.tumblr.com)  
**NSFW Blog:** [@furcock](http://furcock.tumblr.com)  
**Twitter:** [soaproap](http://twitter.com/soaproap)  
**FurAffinity:** [@goodpuns](http://furaffinity.net/user/goodpuns)  
**Instagram:** [@soaproap](https://www.instagram.com/soaproap/)  
**email+hangouts:** soaproap@gmail.com  
**jabber:** soaproap@dukgo.com  
**skype:** goodpuns  
**kik:** onefurry (not currently logged in)  
**slack:** contact me and iI’ll invite you to the group chat!
